const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Category = require("./models/Category");
const Product = require("./models/Product");
const User = require("./models/User");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [carsCategory, petsCategory, computersCategory] = await Category.create({
    title: 'Cars'
  }, {
    title: 'Pets'
    },{
    title: 'Computers'
  });

  const [user1,user2,user3]=await User.create({
    username:'john',
    password: 'doe',
    display_name: 'John Doe',
    phone: '+(996)554 45 45 45',
    token: nanoid(),
  }, {
    username:'sam',
    password: 'smith',
    display_name: 'Sam Smith',
    phone: '+(996)554 54 54 54',
    token: nanoid(),
  },{
    username:'dan',
    password: '123',
    display_name: 'Danny',
    phone: '+(996)555 55 55 55',
    token: nanoid(),
  });

  await Product.create({
    title: 'HP EliteBook 820',
    price: 30000,
    category: computersCategory,
    image: 'fixtures/hp_eliteBook.jpg',
    user:user2,
    description: "Absolutely new notebook",
  },{
    title: 'Asus VivoBook S15',
    price: 90000,
    category: computersCategory,
    image: 'fixtures/asus.jpeg',
    user: user1,
    description: "Information about asus will be here",
  },{
    title: 'Mercedes Benz G 63 ',
    price: 3000000,
    category: carsCategory,
    image: 'fixtures/g63.jpeg',
    user: user1,
    description: 'Mercedes-AMG G63 comes with a twin-turbocharged 4.0-liter V-8 engine that makes 577 horsepower.',
  },{
    title: 'Doberman',
    price: 3500,
    category: petsCategory,
    image: 'fixtures/doberman.jpg',
    user:user2,
    description:'A strongly built, relatively long-bodied dog.',
  },{
    title: 'Lexus RX 450',
    price: 2000000,
    category: carsCategory,
    image: 'fixtures/lx.jpg',
    user: user2,
    description: 'The RX 450hL has a starting MSRP of $51,210. Its main addition over the base 450h is a power-folding third row of seats.',
  },{
    title: 'Bentley Continental GT',
    price: 5000000,
    category: carsCategory,
    image: 'fixtures/bentley.jpg',
    user:user3,
    description: 'The Bentley Continental GT is an all-wheel drive and features a powerful and dynamic 12-cylinder engine.'
  },{
    title: 'German Shepherd',
    price: 2000,
    category: petsCategory,
    image: 'fixtures/german.jpg',
    user:user3,
    description:'A strongly built, relatively long-bodied dog.',
  }, {
    title: 'Mac Book Pro',
    price: 100000,
    category: computersCategory,
    image: 'fixtures/macbook.jpeg',
    user:user3,
    description: "Some descriptions will be here",
  },{
    title: 'Rottweiler',
    price: 2500,
    category: petsCategory,
    image: 'fixtures/rottweiler.jpg',
    user:user1,
    description:'A strongly built, relatively long-bodied dog.',
  });



  await mongoose.connection.close();
};

run().catch(console.error);