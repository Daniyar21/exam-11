const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Product = require('../models/Product');
const auth = require("../middleware/auth");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};

    if (req.query.category) {
      query.category = req.query.category;
    }

    const products = await Product.find(query).populate('category', 'title ')
    res.send(products);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const product = await Product.findById(req.params.id).populate('user','display_name phone').populate('category','title');

    if (product) {
      res.send(product);
    } else {
      res.status(404).send({error: 'Product not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.post('/', [auth,upload.single('image')], async (req, res) => {
  try{

  const productData = {
    title: req.body.title,
    price: req.body.price,
    category: req.body.category,
    description: req.body.description,
    user: req.user._id,
    image: 'uploads/'+req.file.filename,
  }

  const product = new Product(productData);

    await product.save();
    res.send(product);
  } catch(e) {
    res.status(400).send(e);
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const product = await Product.findByIdAndDelete(req.params.id);

    if (product) {
      res.send(`Product '${product.title} removed'`);
    } else {
      res.status(404).send({error: 'Product not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;