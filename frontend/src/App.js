import Layout from "./components/UI/Layout/Layout";
import {Route, Switch} from "react-router-dom";
import Products from "./containers/Products/Products";

import NewProduct from "./containers/NewProduct/NewProduct";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Product from "./containers/Product/Product";

const App = () => (
    <Layout>
        <Switch>
            <Route path="/" exact component={Products}/>
            <Route path="/products/new" component={NewProduct}/>
            <Route path="/products/:id" component={Product}/>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
        </Switch>
    </Layout>
);

export default App;
