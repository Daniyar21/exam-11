import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchProduct} from "../../store/actions/productsActions";
import {Box, makeStyles, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";

const useStyles = makeStyles({
    image: {
        width: '350px',
        height: 'auto'
    }
})

const Product = ({match}) => {
    const classes=useStyles();
  const dispatch = useDispatch();
  const product = useSelector(state => state.products.product);
    console.log(product)
  let cardImage;
 if(product){
     cardImage=apiURL+'/'+product.image;
 }

  useEffect(() => {
    dispatch(fetchProduct(match.params.id));
  }, [dispatch, match.params.id]);

  return product && (
    <Paper component={Box} p={2}>
        <Typography variant="h5"><b>{product.title}</b></Typography>
        <Typography variant="h6">Price: <b>{product.price} KGS</b></Typography>
        <Typography variant="body1">Info: <b>{product.description}</b></Typography>
        <Typography variant="body1">Category: <b>{product.category.title}</b></Typography>
            <img src={cardImage} className={classes.image} alt='product'/>
      <Typography variant="body1">Contacts:</Typography>
      <Typography variant="body1">Seller: <b>{product.user.display_name}</b></Typography>
        <Typography variant="body1">Phone number: <b>{product.user.phone}</b></Typography>
    </Paper>
  );
};

export default Product;