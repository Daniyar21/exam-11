import React, {useEffect, useState} from 'react';
import {Menu, MenuItem} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {fetchCategories} from "../../../../store/actions/categoriesActions";

const CategoriesMenu = () => {
    const dispatch = useDispatch();
    const categories= useSelector(state => state.categories.categories)
    const [anchorEl, setAnchorEl] = useState(null);

    useEffect(()=>{
        dispatch(fetchCategories())
    },[dispatch]);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <> <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} color={"inherit"}>
          Categories
        </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                {categories.map(c=>(
                    <MenuItem
                        key={c._id}
                        component={Link}
                        to={`?category=${c._id}`}
                    >
                        {c.title}</MenuItem>
                ))}
                <MenuItem component={Link} to={'/'}>All</MenuItem>
            </Menu>
        </>
    );
};

export default CategoriesMenu;