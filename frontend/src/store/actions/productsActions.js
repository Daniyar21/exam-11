import axios from "axios";
import {toast} from "react-toastify";
import WarningIcon from '@material-ui/icons/Warning';
import axiosApi from "../../axiosApi";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const FETCH_PRODUCT_REQUEST = 'FETCH_PRODUCT_REQUEST';
export const FETCH_PRODUCT_SUCCESS = 'FETCH_PRODUCT_SUCCESS';
export const FETCH_PRODUCT_FAILURE = 'FETCH_PRODUCT_FAILURE';

export const CREATE_PRODUCT_REQUEST = 'CREATE_PRODUCT_REQUEST';
export const CREATE_PRODUCT_SUCCESS = 'CREATE_PRODUCT_SUCCESS';
export const CREATE_PRODUCT_FAILURE = 'CREATE_PRODUCT_FAILURE';

export const DELETE_PRODUCT_REQUEST = 'DELETE_PRODUCT_REQUEST';
export const DELETE_PRODUCT_SUCCESS = 'DELETE_PRODUCT_SUCCESS';
export const DELETE_PRODUCT_FAILURE = 'DELETE_PRODUCT_FAILURE';



export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, payload: products});
export const fetchProductsFailure = () => ({type: FETCH_PRODUCTS_FAILURE});

export const fetchProductRequest = () => ({type: FETCH_PRODUCT_REQUEST});
export const fetchProductSuccess = product => ({type: FETCH_PRODUCT_SUCCESS, payload: product});
export const fetchProductFailure = () => ({type: FETCH_PRODUCT_FAILURE});

export const createProductRequest = () => ({type: CREATE_PRODUCT_REQUEST});
export const createProductSuccess = () => ({type: CREATE_PRODUCT_SUCCESS});
export const createProductFailure = error => ({type: CREATE_PRODUCT_FAILURE,payload:error});

export const deleteProductRequest = () => ({type: DELETE_PRODUCT_REQUEST});
export const deleteProductSuccess = (id) => ({type: DELETE_PRODUCT_SUCCESS,payload:id});
export const deleteProductFailure = error => ({type: DELETE_PRODUCT_FAILURE,payload:error});


export const fetchProducts = (query) => {
  return async (dispatch) => {
    try {

      dispatch(fetchProductsRequest());
      const response = await axiosApi.get('/products'+query);
      dispatch(fetchProductsSuccess(response.data));
    } catch (error) {
      dispatch(fetchProductsFailure());
        toast.error('Could not fetch products!', {
          theme: 'colored',
          icon: <WarningIcon/>
        });
      }
  };
};

export const fetchProduct = id => {
  return async dispatch => {
    try {
      dispatch(fetchProductRequest());
      const response = await axios.get('http://localhost:8000/products/' + id);
      dispatch(fetchProductSuccess(response.data));
    } catch (e) {
      dispatch(fetchProductFailure());
    }
  };
};

export const createProduct = productData => {
  return async (dispatch, getState) => {
    try {
      const headers = {
        'Authorization': getState().users.user && getState().users.user.token
      };
      dispatch(createProductRequest());
      await axios.post('http://localhost:8000/products', productData, {headers});
      dispatch(createProductSuccess());
      toast.success('Product created');
    } catch (error) {
      if (error.response && error.response.data) {
        toast.error(error.response.data.message);
      } else {
        toast.error('no internet');
      }
    }
  };
};

export const deleteProduct = (id) => {
  return async (dispatch) => {
    try {
      dispatch(deleteProductRequest());
  await axiosApi.delete('/products/'+id);
      dispatch(deleteProductSuccess(id));
    } catch (error) {
      dispatch(deleteProductFailure(error));
    }
  };
};